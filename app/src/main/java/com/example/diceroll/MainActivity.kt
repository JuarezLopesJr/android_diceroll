package com.example.diceroll

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.diceroll.R.drawable.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnRoll.setOnClickListener {
            rollDice()
        }
    }

    private fun rollDice() {
        val drawableResource = when (Random.nextInt(6)) {
            1 -> dice_1
            2 -> dice_2
            3 -> dice_3
            4 -> dice_4
            5 -> dice_5
            else -> dice_6
        }
        diceImage.setImageResource(drawableResource)
    }
}
